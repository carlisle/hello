// 'Hello World!' program 
// g++ hello.cpp -o hello-cpp
  
#include <iostream>
#include <string>
using namespace std;

void say(string);
   
int main()
{
    string message = "World";

//    cout << "Hello, " << message << '!' << endl;
    say(message);
    return 0;
}

void say(string str)
{
    cout << "Hello, " << str << '!' << endl;
}

