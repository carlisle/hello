// usage:
// javac hello.java; java HelloWorldApp 
// or
// javac hello.java; jar cfmv hello.jar hello-manifest.txt HelloWorldApp.class; java -jar hello.jar
// or
// gcj hello.java --main=HelloWorldApp  -o hello-java
// https://github.com/macagua/example.java.helloworld
// https://www.michael-thomas.com/tech/java/javaadvanced/jar/tutorial_firststeps_jar/index.html
// https://www.geeksforgeeks.org/jar-files-java/

public class MessageObj 
{
    private String str;

    public MessageObj ( String inputstr )
    {
        this.str = inputstr;
    }

    public String say()
    {
//        return str;
        System.out.println("Hello, " + str + "!");
    }

    public void load( String name )
    {
        this.str = name;
    }



//public class HelloWorldApp {
    public static int main( String args[] ) 
    {
//        System.out.println("Hello World!"); // Display the string.

        String outputmsg = new String("World");

        MessageObj message = new MessageObj(outputmsg);
//        message.load(outputmsg);
        message.say();

    }
//}

}

//Message message = new Message();
//message.load("World");
//message.say();
