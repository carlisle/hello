// 'Hello World!' program 
// g++ hello.cpp -o hello-cpp
  
#include <iostream>
#include <string>
using namespace std;
   
int main()
{
    string message = "World";

    cout << "Hello, " << message << '!' << endl;
    return 0;
}

