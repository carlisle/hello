// 'Hello World!' program 
// g++ hello.cpp -o hello-cpp

#include <cstdlib>  
#include <iostream>
#include <string>
using namespace std;

class MessageObj {
    public:
    string str;

    MessageObj (string x) {
        str = x;
    }

    void say() {
        cout << "Hello, " << str << '!' << endl;
    }
};

   
int main() {
    MessageObj message("World");
    message.say();

    return EXIT_SUCCESS;
}

