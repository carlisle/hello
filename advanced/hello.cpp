// 'Hello World!' program 
// g++ hello.cpp -o hello-cpp
  
#include <iostream>
#include <string>
   
int main()
{
    char message[] = "World";

    std::cout << "Hello, " << message << '!' << std::endl;
    return 0;
}

