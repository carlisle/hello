#!/usr/bin/env python3

def say (str):
	"This prints a passed string"
	#print( "Hello,", str, "!")
	print( 'Hello, {0}!'.format(str) )


message = "World"

say( message )
