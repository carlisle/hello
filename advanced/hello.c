/* gcc hello.c -o hello-c  */

#include <stdio.h>
#include <stdlib.h>

void say (char s[])
{
    printf("Hello, %s!\n", s);
} 


int main()
{
    char message[] = "World";

    say(message);
    return EXIT_SUCCESS;
}
