! gfortran hello.f90 -o hello-f90
! https://en.wikibooks.org/wiki/Fortran/strings

function func(i) result(j)
    integer, intent(in) :: i ! input
    integer             :: j ! ouput
    j = i**2 + i**3
end function func

program hello

    implicit none
    integer :: a
    integer :: func

    character(len=5) :: message


    a = 3
    print *, "sum of the square and cube of", a, " is", func(a)

    message = "World"

    print *, "Hello, World!"

    write(*,*) "Hello, "//message//"!"

end program hello
