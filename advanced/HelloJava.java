// usage:
// javac HelloJava.java; java HelloJava
// or
// javac HelloJava.java; jar cfmv hello.jar HelloJava-manifest.txt HelloJava.class; java -jar hello.jar
// or
// gcj HelloJava.java --main=HelloJava  -o hello-java
// gcj deprecated on RHEL7+
// https://github.com/macagua/example.java.helloworld
// https://www.michael-thomas.com/tech/java/javaadvanced/jar/tutorial_firststeps_jar/index.html
// https://www.geeksforgeeks.org/jar-files-java/

public class HelloJava 
{
    private String str = null;

    public HelloJava ( String inputstr )
    {
        str = inputstr;
    }

    public void say()
    {
        System.out.println("Hello, " + str + "!");
    }


    public static void main( String[] args ) 
    {
        HelloJava message = new HelloJava( "World" );
        message.say();
    }
}

