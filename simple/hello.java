// usage:
// javac hello.java; java HelloWorld 
// or
// javac hello.java; jar cfmv hello.jar hello-java-manifest.txt HelloWorld.class; java -jar hello.jar
// or
// gcj hello.java --main=HelloWorld  -o hello-java
// https://github.com/macagua/example.java.helloworld
// https://www.michael-thomas.com/tech/java/javaadvanced/jar/tutorial_firststeps_jar/index.html
// https://www.geeksforgeeks.org/jar-files-java/

class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!"); // Display the string.
    }
}

