// https://gobyexample.com/hello-world
// $ go run hello-world.go
// Sometimes we’ll want to build our programs into binaries. We can do this using go build.
// $ go build -o hello-go hello.go
// We can then execute the built binary directly.
// $ ./hello-go

package main

import "fmt"

func main() {
    fmt.Println("Hello, 世界")
}
