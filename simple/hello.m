//  gcc -lobjc hello.m -o hello-objc

// https://www.maheshsubramaniya.com/article/hello-world-in-objective-c-and-compiling-with-gcc.html

#import <stdio.h>

int main( int argc, const char *argv[] ) {
    printf( "Hello World!\n" );
    return 0;
}
