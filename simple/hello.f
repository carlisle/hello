      PROGRAM HELLO
C Hello World in Fortran 77
C https://web.stanford.edu/class/me200c/tutorial_77/03_basics.html
C Lines must be 6 characters indented in F77
C To compile: gfortran -std=legacy hello.f -o hello-f77
C Older: f77 hello.f -o hello-f77
C Older: f2c hello.f; cc -o hello-f77 hello.c libf2c.a
      IMPLICIT NONE
C
      CHARACTER*32 TEXT
C
      TEXT = 'Hello World'
      WRITE (*,*) TEXT
C
      END PROGRAM HELLO
