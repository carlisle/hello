#!/usr/bin/env perl

# The traditional first program.
# perl ./hello.pl
 
# Strict and warnings are recommended.
use strict;
use warnings;
  
# Print a message.
print "Hello, World!\n";
