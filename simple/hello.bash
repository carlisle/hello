#!/usr/bin/env bash
# https://stackoverflow.com/questions/5725296/difference-between-sh-and-bash
# https://www.gnu.org/software/bash/manual/html_node/Major-Differences-From-The-Bourne-Shell.html
# declare STRING variable
string="Hello World !"
#print variable on a screen
printf "%s\n" "${string}"
