/* Hello World in C, K&R-style 
The C Programming Language 
Brian Kernighan, Dennis Ritchie, 1978, p6

cc hello-kr.c; ./a.out
*/

#include <stdio.h>

main()
{
	printf("hello, world\n");
}
