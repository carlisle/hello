-- gnatmake hello.adb -o hello-ada

with Ada.Text_IO; use Ada.Text_IO;
procedure Hello is
begin
  Put_Line("Hello, world!");
end Hello;
