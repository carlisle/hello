// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/hello-world-your-first-program?tabs=windows
// http://www.openkb.info/2016/02/how-to-compile-mono-and-run-c-code-on.html
// How to compile under Mono
// mcs  hello.cs
// mono hello.exe
// Using .Net Core 2.2 on RedHat:
// https://access.redhat.com/documentation/en-us/net_core/2.2/html/getting_started_guide/gs_install_dotnet

using System;
namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, world!");
        }
    }
}
