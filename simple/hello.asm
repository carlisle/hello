; https://github.com/xDaxHarvesterx/assembly-hello-world/blob/master/Hello%20World.asm
; https://jameshfisher.com/2018/03/10/linux-assembly-hello-world/
; https://stackoverflow.com/questions/34906910/difference-between-eax-1-and-ebx-1-in-assembly
; instructions:
; nasm -f elf64 -o hello-asm.o hello.asm
; ld -o hello-asm hello-asm.o

global _start
section .text

_start:
mov edx,len	; size of string in msg
mov ecx,msg	; string defined below
mov ebx,1	; set file descriptor to stout
mov eax,4	; set system call number to sys_write
int 0x80	; call kernel
mov eax,1	; set system call number to sys_exit
int 0x80	; call kernel

section .data
msg db 'Hello World!',0xa
len equ $ - msg
